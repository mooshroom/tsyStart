/*
 {{name}} 内在灵魂，沉稳坚毅
 生成时间：{{date}}   破门狂人R2-D2为您服务！
 */
define('{{en}}', [
    'avalon',
    'text!../../package/{{en}}/{{en}}.html',
    'css!../../package/{{en}}/{{en}}.css',
    '../../plugins/isIt/isIt.js',
    //'../../obj/bridge/obj.js'
], function (avalon, html, css,isIt,obj) {



        var base = {}

        //混入字典表 从window上，实际dic对象位于index.js
        if (dic != undefined) {
            avalon.mix(base, dic)
        }

        //混入对象字段
        if (obj != undefined) {
            base.info = obj.obj
        }


    var vm = avalon.define(avalon.mix({
        $id: "{{en}}",
        ready: function (i) {


            vm.reset()
            index.html = html

            //以及其他方法
            //自动登陆
            Gate.comeIn({
                haveLogin: function (res) {
                    //todo 跳转 (根据实际需求调整)

                },
            })


        },
        reset: function () {
            avalon.mix(vm, {
                //要重置的东西最后都放回到这里
                info: {
                    Account: '',
                    Pwd: ''
                }
            })
        },
        info: {
            Account: '',
            Pwd: ''
        },
        login: function () {
            var data = {
                Account: '',
                Pwd: ''
            }
            avalon.mix(data, vm.info)
            //TODO 验证账户名不可为空
            //if(!isIt.pwd(data.Pwd,'密码')){
            //    return
            //}


            if (data.Account == '') {
                tip.on("用户名不能为空")
                return
            }
            if (data.Pwd == "") {
                tip.on("密码不能为空")
                return
            }
            if (data.Account.length > 6) {
                tip.on("用户名过长")
                return
            }
            $$.call({
                i: "User/login",
                data: data,
                success: function (res, origin) {

                    cache.go(res)

                },
                error: function (err) {
                    tip.on(err)
                }
            })
        }

    },base))
    return window[vm.$id] = vm
})