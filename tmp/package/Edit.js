/*
 {{name}} 内在灵魂，沉稳坚毅
 生成时间：{{date}}   破门狂人R2-D2为您服务！
 */
define('{{en}}', [
    'avalon',
    'text!../../package/{{en}}/{{en}}.html',
    'css!../../package/{{en}}/{{en}}.css'
    //'../../obj/bridge/obj.js'
], function (avalon, html, css, obj) {

    var base = {}

    //混入字典表 从window上，实际dic对象位于index.js
    if (dic != undefined) {
        avalon.mix(base, dic)
    }

    //混入对象字段
    if (obj != undefined) {
        base.info = obj.obj
    }

    var vm = avalon.define(avalon.mix({
        $id: "{{en}}",
        ready: function (i) {


            vm.reset()
            index.html = html

            //以及其他方法
            /*
             * 如果i为0则为添加
             * 如果i<0则为编辑，并且I为ID
             * */


            if (i > 0) {
                //编辑初始化
                vm.ID = i
                vm.getDetails(i)
            } else if (i == 0) {
                //添加初始化
                vm.addInit()
            } else {
                //错误参数跳转添加页面
                goto('#!/{{en}}/0')
            }


        },
        reset: function () {
            avalon.mix(vm, {
                status: '',
                ID: "",
            })
        },
        addInit: function () {

        },
        status: '',
        ID: "",
        getDetails: function (id) {
            obj.get(id, function (res) {
                vm.info = res
            }, function (err) {
                tip.on(err)
            })
        },
        info: {},
        add: function () {

            var data = {
                //todo 填入要参与添加编辑的字段到data中
            }
            //加载基础数据
            ForEach(data, function (el, key) {
                data[key] = vm.info[key]
            })

            //todo 验证必填,填入必填参数到must中
            var must = [
                //格式举例：
                //{
                //    name:'名称',
                //    en:"Name"
                //}
            ]
            for (var i = 0; i < must.length; i++) {
                if (data[must[i].en] == '') {
                    tip.on('还未填写' + must[i].name)
                    return
                }
            }
            //其他验证


            //判断为保存
            if (vm.ID > 0) {
                //调用保存方法

                obj.save(vm.ID, data, function (res) {
                    tip.on('{{name}}保存成功', 1)

                })


                return
            }

            //判断为新建
            obj.add(data, function (res) {
                    tip.on('{{name}}创建成功', 1)



            }, function (err) {
                tip.on(err)
            })
        },

        del: function (i) {
            obj.del(i, function () {
                tip.on('删除成功',1)
                vm.reset()
            }, function (err) {
                tip.on(err)
            })
        }

    }, base))
    return window[vm.$id] = vm
})