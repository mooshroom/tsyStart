/*
 {{name}} 内在灵魂，沉稳坚毅
 生成时间：{{date}}   破门狂人R2-D2为您服务！


 ！！！所有被注释掉的代码都需要在如果列表有筛选的情况下根据实际需要修改后打开！！！
 */
define('{{en}}', [
    'avalon',
    'text!../../package/{{en}}/{{en}}.html',
    'css!../../package/{{en}}/{{en}}.css',
    '../../lib/pager/pager.js'
    //'../../obj/bridge/obj.js'
], function (avalon, html, css, pager, obj,dic) {

    var base = {}

    //混入字典表 从window上，实际dic对象位于index.js
    if (dic != undefined) {
        avalon.mix(base, dic)
    }

    //混入对象字段
    //if (obj != undefined) {
    //    base.info = obj.obj
    //}

    //开始构建vm
    var vm = avalon.define(avalon.mix({
        $id: "{{en}}",
        ready: function (i) {

            //解析参数
            /*
             * 可能的参数格式:P&&keywords&&status[]
             * 例如：1&&keywords&&1_2_3
             * */
            vm.params = i
            var params = String(i).split("&&")


            vm.reset(params);
            index.html = html;

            //置入参数
            if (params[0] >= 1) {
                vm.search(vm.P)
            } else {
                goto('#!/{{en}}/' + vm.buildParams([1]))
            }


        },
        reset: function (params) {
            avalon.mix(vm, {
                //要重置的东西最后都放回到这里
                P: params[0],
            })
        },
        params: '',
        buildParams: function (newParams) {
            var params = String(vm.params).split("&&")

            ForEach(params, function (el, index) {
                if (newParams[index] != undefined) {
                    params[index] = newParams[index]
                }
            })
            return params.join("&&")
        },
        P: 1,
        N: 5,
        T: 150,
        $pager: {
            id: "{{en}}Pager",
            N: 5,
            showPage: 6,//显示多少页
            getList: function (p) {
                goto('#!/{{en}}/' + vm.buildParams([p]))
            }
        },

        list: [],
        search: function (p) {
            var data = {
                P: p,
                N: vm.N,

            }


            obj.search(data, {
                success: function (res) {
                    //假设没有数据，重置各种东西
                    avalon.mix({{en}}Pager, {
                        T: 0,
                        P: vm.P
                    });
                    {{en}}Pager.build(vm.P)
                    vm.list = []

                    //填充返回数据
                    vm.list = res.L
                    vm.P = res.P
                    avalon.mix({{en}}Pager, {
                        T: res.T,
                        P: res.P
                    });
                    {{en}}Pager.build(res.P)
                }
            })



        },


    }, base))
    return window[vm.$id] = vm
})