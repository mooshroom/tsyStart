/*
 {{name}} 内在灵魂，沉稳坚毅
 生成时间：{{date}}   破门狂人R2-D2为您服务！
 */
define('{{en}}', [
    'avalon',
    'text!../../package/{{en}}/{{en}}.html',
    'css!../../package/{{en}}/{{en}}.css',
    //'../../obj/bridge/obj.js'
], function (avalon, html, css, obj) {

    var base = {}

    //混入字典表 从window上，实际dic对象位于index.js
    if (dic != undefined) {
        avalon.mix(base, dic)
    }

    //混入对象字段
    if (obj != undefined) {
        base.info = obj.obj
    }

    var vm = avalon.define(avalon.mix({
        $id: "{{en}}",
        ready: function (i) {

            vm.reset()
            index.html = html

            //以及其他方法
            vm.getInfo(i)


        },
        reset: function () {
            avalon.mix(vm, {
                //要重置的东西最后都放回到这里
                info: {}
            })
        },
        info: {},
        getInfo: function (i) {
            obj.get(i, function (res) {
                    vm.info = res

            }, function (err) {
                tip.on(err)
            })

        }

    }, base))
    return window[vm.$id] = vm
})