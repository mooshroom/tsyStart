/*
 {{en}} 内在灵魂，沉稳坚毅
 生成时间：{{date}}   破门狂人R2-D2为您服务！
 */
define('{{en}}',[
    'avalon',
    'text!../../lib/{{en}}/{{en}}.html',
    'css!../../lib/{{en}}/{{en}}.css'
], function (avalon, html, css) {
    avalon.component('tsy:{{lowerEn}}', {
        $template: html,
        id:"",
        /*
        * ***************参数队列区**************************/



        /*
         * ***************函数空位**************************/




        /*
         * ***************自启动项目**************************/
        $init: function (vm, elem) {
            //主动读取配置
            var elem = avalon(elem)
            var params='id'//todo 参数的名称
            //将参数放入vm对应的地方
            try {
                if(elem.data(params)!=undefined){
                    vm[params] = elem.data(params)
                }
            } catch (err) {}

            /*****初始化方法区域*******************/



            /*****初始化方法区域 end*******************/

            //如果有ID则暴露到window对象下
            if(vm.id!=""){
                window[vm.id]=vm
            }
        },
        $ready: function (vm, elem) {
            //构建完毕后执行的方法
        },


    })
})