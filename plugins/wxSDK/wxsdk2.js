/**
 * Created by mooshroom on 2017/4/4.
 */
define('wxsdk',[
    'avalon',
    'mmRequest',
],function (avalon,mmRequest) {
    //开启进度条
    return function () {
        try {
            pb.startT()
        } catch (err) {
        }

        var config={
            success:function (config) {
                //开始配置微信js-sdk
                wx.config(config);

            },
            error:function (err) {
            }
        }

        //获取配置并触发成功回调

        var url=window.location.protocol+"//r.tansuyun.cn/w.php?i=Js/config"
        var rtsy=cache.go('rtsy')
        if(rtsy!=undefined&&String(rtsy).length>0&&rtsy!='undefined'){
            url+='&tsy=' + rtsy;
        }
        avalon.ajax({
            url:url,
            type:"post",
            data:{
                ID:"gh_ff2bfffc0671",
                APIs:[
                    'checkJsApi',
                    'onMenuShareTimeline',
                    'onMenuShareAppMessage',
                    'onMenuShareQQ',
                    'onMenuShareWeibo',
                    // 'hideMenuItems',
                    // 'showMenuItems',
                    // 'hideAllNonBaseMenuItem',
                    // 'showAllNonBaseMenuItem',
                    // 'translateVoice',
                    // 'startRecord',
                    // 'stopRecord',
                    // 'onRecordEnd',
                    // 'playVoice',
                    // 'pauseVoice',
                    // 'stopVoice',
                    // 'uploadVoice',
                    // 'downloadVoice',
                    'chooseImage',
                    'previewImage',
                    'uploadImage',
                    'downloadImage',
                    'getNetworkType',
                    'openLocation',
                    'getLocation',
                    // 'hideOptionMenu',
                    // 'showOptionMenu',
                    'closeWindow',
                    'scanQRCode',
                    // 'chooseWXPay',
                    // 'openProductSpecificView',
                    // 'addCard',
                    // 'chooseCard',
                    // 'openCard'
                ],
                Debug:false,
                URL:window.location.href
            },
            success:function (res) {
                if(!res.err&&res.d !== false){
                    //执行成功



                    var cfg={
                        debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                        appId: '', // 必填，公众号的唯一标识
                        timestamp: "", // 必填，生成签名的时间戳
                        nonceStr: '', // 必填，生成签名的随机串
                        signature: '',// 必填，签名，见附录1
                        jsApiList: [] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                    }

                    ForEach(cfg,function (el,key) {
                        cfg[key]=res.d[key]
                    })

                    cfg.debug=false

                    config.success(cfg)

                    cache.go({
                        rtsy:res.tsy
                    })
                }
                else{
                    //执行失败
                    config.error(res.err)
                    console.log('服务端执行错误：'+res.m)
                    console.log(res)
                }

                try {
                    pb.endT()
                } catch (err) {
                }

            }
        })
    }

})