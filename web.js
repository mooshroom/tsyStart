/**
 * Created by mooshroom on 2017/1/11.
 */


/*………………………………………………………………………………………………全局配置………………………………………………………………………………………………*/
var dic= {
    //    车型字典
    $carDic: {1: 'A级车', 2: "B级车", 3: "商务车"},
    //    行程字典
    $TypeDic: {1: '短途', 2: '长途', 3: '接机', 4: '包车'},
    //    性别
    $SexDic:{0:'女',1:'男'}
}
//var apiURL = './index.php?i=';
var apiURL='http://mychengyuan.dev.tansuyun.cn/index.php?i='
//alert("开始请求3" + apiURL);
// $$.call({
//     i: 'Wechat/getJSTicket',
//     data: {},
//     success: function (res) {
//         //console.log(res)
//         //alert(res)
//         //try{
//         wxgetReady(res.ticket);
//         //}catch(err){err.message}
//     }
// })


/*………………………………………………………………………………………………index的视图模型………………………………………………………………………………………………*/
require([
    'avalon',
    'mmRequest',
    //'../../plugins/door/door.js',
    'text!../../nav.json',
    '../../lib/pop/popAlert.js',
], function (avalon, mmRequest, nav) {
    var vm = avalon.define({
        $id: "index",
        ready: function () {

            console.time("门禁配置时间");
            require(['../../plugins/Gate/Gate'], function () {
                window.Gate = newGate({
                    autoLoginAPI: "Diet/User/reLogin",
                    haveLogin: function (res) {
                        console.log(1111)
                    },
                    notLogin: function (res) {
                        console.log(222)

                    },
                })
                console.timeEnd("门禁配置时间");

            })

            console.time("路由加载时间");
            //构建路由
            require([
                "mmRouter",
            ], function () {
                console.timeEnd("路由加载时间");
                avalon.router.get("/", function () {
                    goto('#!/Login/0')
                });
                var navList = JSON.parse(nav).nav;
                //构建导航的路由
                getMap(navList);
                console.log("路由构建完毕")
                //开始监听
                avalon.history.start();


                //vm.toggleSide(0)
                avalon.scan();

                console.time("缓存设置时间");
                vm.uid = cache.go('UID')
                vm.un = cache.go('Name')

                var g = cache.go('G')
                if (g == undefined) {
                    vm.G = []
                    return
                }
                vm.G = addUp(cache.go('G').split(','))
                console.timeEnd("缓存设置时间");
            })

            vm.$watch('html', function () {
                vm.contentHeight = 0
                vm.contentOpacity = 0
                vm.contentMT = -300
                setTimeout(function () {
                    vm.contentHeight = 'auto'
                    vm.contentOpacity = 1
                    vm.contentMT = 0
                }, 250)
            })


        },
        reset: function () {

        },
        lib: "",
        //当前用户的信息
        contentMT: "0",
        contentHeight: 'auto',
        contentOpacity: 1,
        admin: false,
        uid: "",
        un: "",
        G: "",
        g: "", //用户切换页面标记

        showLoginBtn:false,


        //登出
        logout: function () {
            $$.call({
                    i: 'User/logout',
                    data: {},
                    success: function (res) {
                        window.location.href = "#!/Login/0"
                        Gate.reset()
                        index.uid = ""
                    },
                    err: function (err) {
                        tip.on(err, 1)
                    }
                }
            )
        },


        // 控制姓名的下拉框
        showNameDropdown: false,
        SNDTimeout: "",
        /*
         * toggleND 控制姓名下拉框的出现和小时
         * @param i ：0-消失；1-出现；2-自动
         * */
        toggleND: function (i) {
            clearTimeout(vm.SNDTimeout)

            switch (i) {
                case 0:
                    vm.SNDTimeout = setTimeout(function () {
                        vm.showNameDropdown = false;
                    }, 400)
                    break
                case 1:
                    vm.showNameDropdown = true;
                    break
                case 2:
                    vm.showNameDropdown = !vm.showNameDropdown
            }
        },


        html: '',
        nowTop: '',

        //导航条
        nav: [{
            name: "",
            en: "",
            href: ""
        }],
        CoachNav: [
            {
                name: "我的主页",
                en: "Home",
                href: "#!/Home/0&&&&"
            },
        ],
        AdminNav: [
            {
                name: "用户管理",
                en: "CoachList",
                href: "#!/CoachList/1&&&&"
            }
        ],

        //组件配置
        //提示框配置
        $opta: {
            id: "tip"
        },

        //websocket配置
//        $optc: {
//            id: "ws",
//            server: "ws://180.97.81.190:46032",//线上版本
////                    server: "ws://my.s.tansuyun.cn:46080",//测试版本
//            debug: false
//        },
        $optd: {
            id: "pb"
        },
        //$optTop: {
        //    id: "toTop"
        //},
        $optpop: {
            id: 'pop',
            width: "960",
        },
        $optpop2: {
            id: 'pop2',
            width: "560",
        },
        foodKey: '',

    })

    console.time('基础组件加载时间')
    require([
        '../../lib/tip/tip.js',
        '../../lib/progressbar/progressbar.js',

    ], function () {
        avalon.nextTick(function () {
            console.timeEnd("基础组件加载时间");
            avalon.scan();
            vm.ready()

            //非必须组件加载
            console.time('额外组件加载')
            require([
                '../../lib/food_search/food_search.js',
                '../../plugins/shortcut/shortcut',], function () {
                console.timeEnd('额外组件加载')

                //setTimeout(testPop,1000)

                TimerMember()
            })
        })

    })

    window.index = vm

    /*………………………………………………………………………………………………路由处理函数………………………………………………………………………………………………*/

    //这个函数用来对用户进行权限控制，未来可能会添加多种限制条件
    function checkLimit(fn, limit) {


        if (cache.go("UnitID") == 23) {
            fn()
        } else {
            tip.on("您的账户没有访问改模块的权限")
            //history.go(-1)
        }

    }

    /*路由*/
    function newRouter(n) {
        console.time(n.name + "路由创建耗时")
        var en = n.en;
        n.vm = '../../package/' + en + "/" + en + '.js'
        avalon.router.get('/' + en + '/:i', function (i) {
            vm.showLoginBtn=false
            console.time('模块打开时间')
            //检查权限
            if (n.only > 0) {
                Gate.comeIn({
                    haveLogin: function (res) {
                        //做权限判断
                        loadVM()
                    },
                    notLogin: function () {
                        goto('#!/login/0')
                    }
                })

            } else {
                Gate.comeIn({
                    haveLogin: function (res) {

                    },
                    notLogin: function () {

                    }
                })
                loadVM()
            }


            function loadVM() {
                //统计当前页面
                //_hmt.push(['_trackPageview', '/#!/'+n.en+'/'+i+'?page='+ n.name])
                //_czc.push(['_trackPageview', '/#!/'+n.en+'/'+i+'?page='+ n.name])
                //开启进度条
                try {
                    pb.startT()
                } catch (err) {

                }

                //if (n.name != "登陆") {
                //    document.getElementById("title").innerText = n.name
                //} else {
                //    document.getElementById("title").innerText = '碳蛋知'
                //}

                //tip.on("正在加载……",1)
                if (n.vm) {
                    require([n.vm], function () {
                        avalon.vmodels[en].ready(i)
                        index.nowTop = n.bePartOf
                        if (n.font) {
                            vm.nav = vm.CoachNav
                            vm.g = 1
                        } else {
                            vm.nav = vm.AdminNav
                            vm.g = 2
                        }
                        //tip.off("正在加载……",1)
                        if (pop.state != 0) {
                            pop.close()
                        }
                        //结束进度条
                        try {
                            pb.endT()
                        } catch (err) {
                        }
                    })
                }
                if (n.fn) {
                    n.fn(i)

                    //结束进度条
                    try {
                        pb.endT()
                    } catch (err) {
                    }
                }


                console.log(n.name + "模块加载完毕")
                console.timeEnd('模块打开时间')
            }

        });
        console.timeEnd(n.name + "路由创建耗时")
    }

    function getMap(nav) {
        console.time("路由写入时间")
        var l = nav
        var ll = l.length
        var lsl;
        for (var i = 0; i < ll; ++i) {
            //直接渲染项目
            newRouter(l[i])
        }
        console.timeEnd("路由写入时间")
    }


})


/*………………………………………………………………………………………………全局函数………………………………………………………………………………………………*/
//跨浏览器事件对象方法
var EventUtil = new Object;
EventUtil.addEventHandler = function (oTarget, sEventType, fnHandler) {
    if (oTarget.addEventListener) {
        oTarget.addEventListener(sEventType, fnHandler, false);
    } else if (oTarget.attachEvent) {
        oTarget.attachEvent("on" + sEventType, fnHandler);
    } else {
        oTarget["on" + sEventType] = fnHandler;
    }
};

EventUtil.removeEventHandler = function (oTarget, sEventType, fnHandler) {
    if (oTarget.removeEventListener) {
        oTarget.removeEventListener(sEventType, fnHandler, false);
    } else if (oTarget.detachEvent) {
        oTarget.detachEvent("on" + sEventType, fnHandler);
    } else {
        oTarget["on" + sEventType] = null;
    }
};

EventUtil.formatEvent = function (oEvent) {
    if (isIE && isWin) {
        oEvent.charCode = (oEvent.type == "keypress") ? oEvent.keyCode : 0;
        oEvent.eventPhase = 2;
        oEvent.isChar = (oEvent.charCode > 0);
        oEvent.pageX = oEvent.clientX + document.body.scrollLeft;
        oEvent.pageY = oEvent.clientY + document.body.scrollTop;
        oEvent.preventDefault = function () {
            this.returnValue = false;
        };

        if (oEvent.type == "mouseout") {
            oEvent.relatedTarget = oEvent.toElement;
        } else if (oEvent.type == "mouseover") {
            oEvent.relatedTarget = oEvent.fromElement;
        }

        oEvent.stopPropagation = function () {
            this.cancelBubble = true;
        };

        oEvent.target = oEvent.srcElement;
        oEvent.time = (new Date).getTime();
    }
    return oEvent;
};

EventUtil.getEvent = function() {
    if (window.event) {
        return this.formatEvent(window.event);
    } else {
        return EventUtil.getEvent.caller.arguments[0];
    }
}


//批量绑定快捷键
function bindK(obj) {
    require(['../../plugins/shortcut/shortcut.js'], function () {
        /*快捷键设置*/

        var x
        for (x in obj) {
            if (x.charAt(0) != "$") {
                if (obj.$opt != undefined) {
                    shortcut.add(x, obj[x], obj.$opt)
                } else {
                    shortcut.add(x, obj[x])
                }

                //console.log(x + "快捷键绑定成功")
            }

        }
    })
}

//批量删除快捷键
function removeK(obj) {
    require(['../../plugins/shortcut/shortcut.js'], function () {
        /*快捷键设置*/

        var x
        for (x in obj) {
            if (x.charAt(0) != "$") {
                shortcut.remove(x)
                //console.log(x + "已解除绑定")
            }

        }
    })
}

//安全相加 把所传入的数组的每一项转化为数值然后相加，返回加的结果
function addUp(arr) {
    var result = 0
    for (var i = 0; i < arr.length; i++) {
        result += Number(arr[i])
    }
    return result
}

//输入框输入限制
function minNumber(el) {
    if (el.value == "" || el.value < 0) {
        el.value = ""
    }
}

/*根据时间戳获取字符串*/
function getDateFromTimestamp(Timestamp) {
    for (var i = Timestamp.length; i < 13; i++) {
        Timestamp += '0';
    }
    var date = new Date();
    date.setTime(Timestamp);

    var month = (date.getMonth() + 1) + ''
    for (var o = month.length; o < 2; o++) {
        month = '0' + month
    }
    var day = date.getDate() + ''
    for (var p = day.length; p < 2; p++) {
        day = '0' + day
    }
    return date.getFullYear() + "-" + month + "-" + day
}


//根据字符串获取时间戳
function newDateAndTime(Str) {
    var dateStr = Str.replace("T", " ")
    var ds = dateStr.split(" ")[0].split("-");
    var ts = dateStr.split(" ")[1] ? dateStr.split(" ")[1].split(":") : ['00', '00', '00'];
    if (ts.length < 3) {
        for (var i = ts.length; i < 3; i++) {
            ts.push('00')
        }
    }
    var r = new Date();
    r.setFullYear(ds[0], ds[1] - 1, ds[2]);
    r.setHours(ts[0], ts[1], ts[2], 0);
    r=r.getTime();
    return r;
}

/*
 * 根据字符串获取时间戳(毫秒)
 * @example:
 * var date="2016-11-16 15:12:12"//"2016-11-16"
 *undefined
 *date = new Date(Date.parse(date.replace(/-/g, "/")));
 *date = date.getTime();
 *1479280332000
 * */
function nowTimeTamp(date){
    date = new Date(Date.parse(date.replace(/-/g, "/")));
    date = date.getTime();
    return date;
}

//将日期转换为可填入input的格式
function T2I(Timestamp) {
    return new Date(+Timestamp+8*3600*1000).toISOString().replace(/.[0-9]{3}Z$/,'')
}

//示例："2017-01-04T08:18"
function T2IS(Timestamp) {
    // 中国标准时间
    return new Date(+Timestamp+8*3600*1000).toISOString().replace(/:[0-9]{2}.[0-9]{3}Z$/,'')
}

//转换为10为时间戳发送给后端
/*
 * s 要进行转换的时间戳
 * u 转换后的时间单位 字符串 'ms' 毫秒 's' 秒
 * */
function timeLengthFormat(s,u){
    switch (u){
        case 'ms':
            return Math.ceil(s*1000)
            break;
        case 's':
            return Math.ceil(s/1000)
            break;
    }
}


//遍历数组和对象
/*
 * for each 语句，
 * 实现for 和for(var i in y)的功能
 * 调用时
 ForEach(obj,function(i){
 })
 * */
function ForEach(obj, func) {
    if (typeof obj == "object") {
        if (obj.length == undefined) {
            for (var x in obj) {
                //传入（每一项，每一项的序列号）
                func(obj[x], x);
            }
        } else {
            for (var i = 0; i < obj.length; i++) {
                //传入（每一项，每一项的序列号）
                func(obj[i], i);
            }
        }
    } else {
        console.log('类型错误:' + JSON.stringify(obj))
    }
}


//界面跳转的封装函数
function goto(href) {
    window.location.href = href
}

//列表类页面的参数构建
function buildListParams(p, k, t) {
    var params = []
    params.push(p)
    params.push(k)
    params.push(t.join("_"))
    return params.join("&&")
}


//安全赋值，用于解决服务端在字段为空时返回的空数组无法复制给原本设计为对象格式的字段问题
function safeMix(to,from){
    ForEach(from, function (el, key) {
        try{
            to[key]=from[key]
        }catch (err){
            console.log(err)
        }
    })

    return to
}