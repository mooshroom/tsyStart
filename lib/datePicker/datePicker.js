/*
 datePicker 内在灵魂，沉稳坚毅
 生成时间：Fri Jan 20 2017   破门狂人R2-D2为您服务！
 */
define('datePicker', [
    'avalon',
    'text!../../lib/datePicker/datePicker.html',
    'css!../../lib/datePicker/datePicker.css'
], function (avalon, html, css) {
    avalon.component('tsy:datepicker', {
        $template: html,
        id: "",
        /*
         * ***************参数队列区**************************/
        input:"",
        algin:'left-top',
        single: true,
        wakening: false,//是否为唤醒状态

        wakeUp: function () {

        },
        callback: function () {

        },
        dates: [{day:'',times:''}],
        week: ['日', '一', '二', '三', '四', '五', '六'],

        year: '',
        month: '',
        date1: {
            year: "",
            month: "",
            day: '',
            times: "",
        },
        date2: {
            year: "",
            month: "",
            day: '',
            times: "",
        },
        /*
         * ***************函数空位**************************/
        //开启或关闭的函数
        toggle: function () {

        },

        //构建日历的函数
        buildDates: function () {

        },

        buildTimeout: '',

        //选择日期的函数
        pickDate: function () {

        },

        //选中本月
        setThisYear: function () {

        },
        //选中本年
        setThisMonth: function () {

        },

        //设置日期
        setNowDate: function (date1, date2) {

        },

        //下个月
        nextMonth: function () {

        },

        nextYear: function () {

        },
        //上个月
        preMonth: function () {

        },
        preYear: function () {

        },

        //取消
        cancel: function () {

        },

        //准备离开
        timeoutLeave:'',
        imleave: function () {

        },
        //取消准备离开
        imback: function () {

        },

        //触发回调
        ok: function () {

        },
        /*
         * ***************自启动项目**************************/
        $init: function (vm, elem) {
            //主动读取配置
            var elem = avalon(elem)
            var params = 'id'//todo 参数的名称
            //将参数放入vm对应的地方
            try {
                if (elem.data(params) != undefined) {
                    vm[params] = elem.data(params)
                }
            } catch (err) {
            }

            //闰年判断
            function RunNian(The_Year) {
                if ((The_Year % 400 == 0) || ((The_Year % 4 == 0) && (The_Year % 100 != 0))) {
                    return true;
                }

                else {
                    return false;
                }

            }

            //计算某月天数
            function monthAllDay(year, month) {
                if (month == 2) {
                    if (RunNian(year)) {
                        return 29
                    } else {
                        return 28
                    }
                }

                var monthDay = {
                    1: 31,
                    3: 31,
                    4: 30,
                    5: 31,
                    6: 30,
                    7: 31,
                    8: 31,
                    9: 30,
                    10: 31,
                    11: 30,
                    12: 31,
                }
                return monthDay[month]

            }

            //计算某天星期几
            function getWeek(year, month, day) {
                return new Date(year, month - 1, day).getDay()
            }

            /*****初始化方法区域*******************/
            vm.imleave= function () {
                vm.timeoutLeave=setTimeout(function () {
                    vm.wakening=false
                },2400)
            }

            vm.imback= function () {
                clearTimeout(vm.timeoutLeave)
            }
            vm.toggle = function () {
                vm.wakening = !vm.wakening
                if (vm.wakening) {
                    //vm.buildDates()
                    //if(vm.type==1){
                    //    vm.setNowDate(vm.date1.times)
                    //}else{
                    //    vm.setNowDate(vm.date1.times, vm.date2.times)
                    //}

                    vm.wakeUp(vm)
                }else{

                }
            }

            vm.setNowDate = function (d1, d2) {
                var dt1, dt2
                if (d2 != undefined && d2 < d1) {
                    //如果后一个时间大于前一个时间
                    dt1 = d2
                    dt2 = d1
                } else {
                    dt1 = d1
                    dt2 = d2
                }
                //根据时间戳获得时间对象，并获得该时间戳的年月
                var year, month

                var date1 = new Date();
                date1.setTime(dt1);
                year = date1.getFullYear()
                month = (date1.getMonth() + 1)

                vm.year = year
                vm.month = month

                var day1 = date1.getDate()
                if (dt2 != undefined&&dt2!=='') {
                    var day2 = new Date(dt2).getDate()
                }


                vm.date1 = {
                    day: day1,
                    times: dt1,
                }
                if (dt2 != undefined) {
                    vm.date2 = {
                        day: day2,
                        times: dt2,
                    }
                } else {
                    vm.date2 = {
                        day: '',
                        times: '',
                    }
                }
            }


            vm.setThisMonth= function () {
                var year,month,d1,d2
                if(vm.date1.times===''){
                    year=vm.year
                    month=vm.month
                }else{
                    year=vm.date1.year
                    month=vm.date1.month
                }
                d1=1
                d2=monthAllDay(year,month)

                //选中日期
                vm.pickDate(year,month,d1)
                vm.pickDate(year,month,d2)

            }

            vm.setThisYear= function () {
                var year
                if(vm.date1.times===''){
                    year=vm.year
                }else{
                    year=vm.date1.year
                }

                //选中日期
                vm.pickDate(year,1,1)
                vm.pickDate(year,12,31)
            }

            vm.buildDates = function () {
                clearTimeout(vm.buildTimeout)
                vm.buildTimeout = setTimeout(function () {
                    var year = vm.year, month = vm.month
                    var allDay = monthAllDay(year, month) //当月天数

                    //获得日期容器
                    var daysList = []

                    //循环放入容器
                    var lv = 0
                    for (var i = 1; i < allDay + 1; i++) {
                        var week = getWeek(year, month, i)
                        daysList[week + lv * 7] = {
                            day:i,
                            times:new Date(year,month-1,i).getTime()
                        }
                        if (week == 6) {
                            lv++
                        }
                    }

                    for(var o=0;o<daysList.length;o++){
                        if(daysList[o]==undefined){
                            daysList[o]={
                                year:"",
                                month:"",
                                day:'',
                                times:''
                            }
                        }
                    }


                    //渲染
                    vm.dates = daysList
                }, 100)



            }

            vm.pickDate = function (year, month, day) {
                if (day === '') {
                    return
                }

                var times = new Date(year, month - 1, day).getTime()
                console.log(times)

                if (times == vm.date1.times || times == vm.date2.times) {
                    //落点等于开始或结束的取消掉
                    if (times == vm.date1.times) {
                        vm.date1 = {
                            year: vm.date2.year,
                            month: vm.date2.month,
                            day: vm.date2.day,
                            times: vm.date2.times,
                        }

                    }
                    vm.date2 = {
                        year: '',
                        month: '',
                        day: "",
                        times: '',
                    }
                } else if (vm.date1.day == ''||vm.single) {
                    //首次选择
                    vm.date1 = {
                        year: year,
                        month: month,
                        day: day,
                        times: times
                    }
                } else if (vm.date2.day == '') {
                    //第二次选择
                    if (times < vm.date1.times) {
                        vm.date2 = {
                            year: vm.date1.year,
                            month: vm.date1.month,
                            day: vm.date1.day,
                            times: vm.date1.times,
                        }
                        vm.date1 = {
                            year: year,
                            month: month,
                            day: day,
                            times: times
                        }
                    } else {
                        vm.date2 = {
                            year: year,
                            month: month,
                            day: day,
                            times: times
                        }
                    }
                } else {
                    //第三次选择
                    if (times < vm.date1.times) {
                        //落点在开始时间之前
                        vm.date1 = {
                            year: year,
                            month: month,
                            day: day,
                            times: times
                        }
                    } else if (times > vm.date1.times && times < vm.date2.times) {
                        //落点在之间
                        vm.date2 = {
                            year: vm.year,
                            month: vm.month,
                            day: day,
                            times: times
                        }
                    } else if (times > vm.date2.times) {
                        //落点在结束时间之后
                        vm.date2 = {
                            year: vm.year,
                            month: vm.month,
                            day: day,
                            times: times
                        }
                    }
                }



                if (vm.single) {
                    //直接返回date1
                    vm.ok()
                }

            }

            vm.ok= function () {
                var t1=''
                if(vm.date1.times!==''){
                    t1= avalon.filters.date(vm.date1.times,'yyyy-MM-dd')
                }
                var t2=''
                if(vm.date2.times!==''){
                    t2=avalon.filters.date(vm.date2.times,'yyyy-MM-dd')
                }
                var dateStr=t1+' 至 '+t2

                if(vm.single){
                    dateStr=t1
                }

                if(t1===''&&t2===''){
                    dateStr=''
                }

                vm.callback(vm,vm.date1.times,vm.date2.times,dateStr)

                //elem.element.children[0].children[2].style.opacity=0
                setTimeout(function () {
                    vm.wakening=false
                },250)

            }

            vm.nextMonth = function () {
                if (vm.month == 12) {
                    vm.month = 1
                    vm.year++
                    return
                }
                vm.month++
            }
            vm.preMonth = function () {
                if (vm.month == 1) {
                    vm.month = 12
                    vm.year--
                    return
                }
                vm.month--
            }
            vm.nextYear= function () {
                vm.year++
            }
            vm.preYear= function () {
                vm.year--
            }

            vm.cancel= function () {
                vm.date1=vm.date2={
                    year:'',
                    month:'',
                    day:'',
                    times:'',
                }
                vm.callback(vm,'','','')
            }


            /*****初始化方法区域 end*******************/

            //如果有ID则暴露到window对象下
            if (vm.id != "") {
                window[vm.id] = vm
            }
        },
        $ready: function (vm, elem) {
            //构建完毕后执行的方法

            ['year', 'month'].forEach(function (el) {
                vm.$watch(el, function (a, b) {
                    vm.buildDates()
                })
            })
        },


    })
})